import { Colors } from './Colors';

export class Theme {
    public static headerColor = Colors.gray0;
    public static headerTitleColor = Colors.brown;
    public static subHeaderColor = Colors.gray2;
    public static subHeaderTitleColor = Colors.purple;
    public static backgroundColor = Colors.gray1;
    public static cardColor = Colors.white;
    public static textColor = Colors.black;
    public static textLightColor = Colors.gray6;
    public static dividerColor = Colors.gray3;
    public static underlineHighlightColor = Colors.yellow;
    public static borderColor = Colors.gray5;
    public static disabledColor = Colors.gray4;
    public static searchBoxColor = Colors.white;
    public static selectHoverColor = Colors.gray1;
    public static selectedColor = Colors.gray2;
}
