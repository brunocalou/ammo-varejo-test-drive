import { IconSize } from '../util/IconSize';
import { Colors } from './Colors';

export class IconTheme {
    public static size = IconSize.normal;
    public static bigSize = IconSize.big;
    public static color = Colors.purpleLight;
}