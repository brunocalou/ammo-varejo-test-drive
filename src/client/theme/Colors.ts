export class Colors {
    public static brown = '#422B18';
    public static purple = '#433E66';
    public static purpleLight = '#8E8CA3';
    public static yellow = '#DFBE7F';
    public static white = '#FFFFFF';
    public static gray0 = '#F9F9F9';
    public static gray1 = '#EEEEEE';
    public static gray2 = '#ECEBEF';
    public static gray3 = '#EDEAE9';
    public static gray4 = '#DDDDE1';
    public static gray5 = '#CBCBCB';
    public static gray6 = '#666666';
    public static black = '#0E0E0E';
}
