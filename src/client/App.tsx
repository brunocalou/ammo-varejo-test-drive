import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home } from './pages/Home/Home';
import { injectGlobal } from 'styled-components';
import { Theme } from './theme/Theme';

export const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Home} />
    </Switch>
  </BrowserRouter>
);

// Global style
// tslint:disable-next-line
injectGlobal`
    body {
        background-color: ${Theme.backgroundColor};
        color: ${Theme.textColor};
        font-family: sans-serif;
        padding: 0;
        margin: 0;
        width: 100%;
        height: 100%;
        * {
            font-weight: 100;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
    }
    html {
        height: 100%;
    }
    #root {
        height: 100%;
    }

    input {
        color: ${Theme.textColor};
    }
`;