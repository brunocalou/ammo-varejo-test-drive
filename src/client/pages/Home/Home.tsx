import * as React from 'react';
import { Header } from '../../components/Header/Header';
import { SubHeader } from '../../components/SubHeader/SubHeader';
import SearchBox from '../../components/SearchBox/SearchBox';
import { HighlightedText } from '../../components/HighlightedText/HighlightedText';
import { ContentWrapper } from '../../components/ContentWrapper/ContentWrapper';
import { ProductList } from '../../components/ProductList/ProductList';
import { Response } from '../../../shared/response';
import { IFormattedProduct } from '../../../shared/formatted/iformatted-product.d';
import { ProductsApi, ISearchParams, searchLimits } from '../../api/ProductsApi';
import * as queryString from 'query-string';
import { History } from 'history';
import { Row } from '../../components/Row/Row';
import { PaginationItems } from '../../components/PaginationItems/PaginationItems';
import Dropdown from 'react-dropdown';
import { StyledDropdownWrapper } from '../../components/StyledDropdownWrapper/StyledDropdownWrapper';

interface IDropdownOption {
    value: any;
    label: string;
}

export interface IHomeProps {
    location: Location;
    history: History;
}

export interface IHomeState {
    response?: Response<IFormattedProduct[]>;
    isFetching: boolean;
    failedToFetch: boolean;
    searchParams: ISearchParams;
    selectedProductsPerPage: IDropdownOption;
}

export class Home extends React.Component<IHomeProps, IHomeState> {
    private productsPerPageDropdownOptions: IDropdownOption[];

    constructor(props: IHomeProps) {
        super(props);

        this.productsPerPageDropdownOptions = this.generateProductsPerPageDropdownOptions();

        this.state = {
            response: null,
            isFetching: false,
            failedToFetch: false,
            searchParams: null,
            selectedProductsPerPage: this.productsPerPageDropdownOptions[0],
        };

        this.search = this.search.bind(this);
        this.reload = this.reload.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.fetchProducts = this.fetchProducts.bind(this);
        this.selectProductsPerPage = this.selectProductsPerPage.bind(this);
    }

    public async componentDidMount() {
        window.addEventListener('popstate', this.fetchProducts, false);
        await this.fetchProducts();
        this.setSelectedProductsPerPageOption();
    }

    public componentWillUnmount() {
        window.removeEventListener('popstate', this.fetchProducts, false);
    }

    public renderBody() {
        return <React.Fragment>
            {
                this.state.searchParams && this.state.searchParams.search
                    ? <SubHeader title={this.state.searchParams.search.split('+').join(' ')} />
                    : <SubHeader title='Lista de produtos' />
            }

            <ContentWrapper>
                <HighlightedText>{this.state.response.metrics.resultsCount} produtos encontrados</HighlightedText>

                <ProductList products={this.state.response.data} />

                <Row>
                    <StyledDropdownWrapper>
                        <Dropdown
                            options={this.productsPerPageDropdownOptions}
                            onChange={this.selectProductsPerPage}
                            value={this.state.selectedProductsPerPage} />
                    </StyledDropdownWrapper>
                    <PaginationItems
                        onPageChange={this.handlePageChange}
                        maxPagesToShow={6}
                        pagination={this.state.response.pagination} />
                </Row>
            </ContentWrapper>
        </React.Fragment>;
    }

    public renderError() {
        return <SubHeader title='Um erro ocorreu. Tente novamente mais tarde' />;
    }

    public renderEmpty() {
        return <SubHeader title='Nada por aqui... tente fazer uma busca com outras palavras' />;
    }

    public renderLoading() {
        return <SubHeader title='Carregando' />;
    }

    public render() {
        return (
            <div>
                <Header title='mmartan' onTitleClick={this.reload}>
                    <SearchBox placeholder='Procurar...' onSearch={this.search} />
                </Header>
                {
                    this.state.isFetching
                        ? this.renderLoading()
                        : this.state.response === null || this.state.response.data === null
                            ? this.renderError()
                            : this.state.response.data.length > 0
                                ? this.renderBody()
                                : this.renderEmpty()
                }
            </div>
        );
    }

    private async handlePageChange(page: number) {
        const parsed = queryString.parse(this.props.location.search);
        parsed.page = page;

        await this.props.history.push({
            pathname: '/',
            search: queryString.stringify(parsed)
        });
        await this.fetchProducts();
    }

    private async selectProductsPerPage(option: IDropdownOption) {
        const parsed = queryString.parse(this.props.location.search);
        parsed.limit = option.value;
        parsed.page = 1;

        await this.props.history.push({
            pathname: '/',
            search: queryString.stringify(parsed)
        });
        await this.fetchProducts();
    }

    private async search(value: string) {
        const query = {
            search: value,
            limit: this.state.selectedProductsPerPage.value
        };

        await this.props.history.push({
            pathname: '/',
            search: queryString.stringify(query)
        });
        await this.fetchProducts();
    }

    private async reload() {
        await this.props.history.push({
            pathname: '/',
        });
        await this.fetchProducts();
    }

    private async fetchProducts() {
        try {
            const params = ProductsApi.normalizeParams(queryString.parse(this.props.location.search));
            this.setState({ searchParams: params });
            this.setState({ failedToFetch: false });
            this.setState({ isFetching: true });
            const response = await ProductsApi.getProducts(params);
            this.setState({ isFetching: false });
            this.setState({ response: response.data });
            this.setSelectedProductsPerPageOption();
        } catch (error) {
            this.setState({ isFetching: false });
            this.setState({ failedToFetch: true });
        }
    }

    private generateProductsPerPageDropdownOptions(): IDropdownOption[] {
        return searchLimits.map(limit => {
            return {
                value: limit,
                label: `${limit} produtos por página`,
            };
        });
    }

    private setSelectedProductsPerPageOption() {
        const parsed = queryString.parse(this.props.location.search);
        const selected =
            this.productsPerPageDropdownOptions
                .find(option => option.value.toString() === parsed.limit)
            || this.productsPerPageDropdownOptions[0];

        this.setState({ selectedProductsPerPage: selected });
    }
}