import * as React from 'react';
import styled from 'styled-components';

export class ContentWrapperSize {
    public static SMALL_VERTICAL = '2em';
    public static SMALL_HORIZONTAL = '2em';
    public static NORMAL_VERTICAL = '2em';
    public static NORMAL_HORIZONTAL = '8em';
}
export enum WrapperSize {
    SMALL,
    NORMAL
}

export interface IContentWrapperProps {
    size?: WrapperSize;
    children: React.ReactNode;
}

export function ContentWrapper(props: IContentWrapperProps) {
    return (
        <Wrapper size={props.size}>
            {props.children}
        </Wrapper>
    );
}

function resolvePadding(size: WrapperSize) {
    if (size === WrapperSize.SMALL) {
        return `${ContentWrapperSize.SMALL_VERTICAL} ${ContentWrapperSize.SMALL_HORIZONTAL}`;
    }

    return `${ContentWrapperSize.NORMAL_VERTICAL} ${ContentWrapperSize.NORMAL_HORIZONTAL}`;
}

const Wrapper = styled.section`
    padding: ${(props: IContentWrapperProps) => resolvePadding(props.size)};
    height: 100%;

    > * {
        display: block;
    }
`;