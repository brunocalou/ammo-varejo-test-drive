import * as React from 'react';
import { IFormattedProduct } from '../../../shared/formatted/iformatted-product.d';
import { ProductItem } from './ProductItem/ProductItem';
import styled from 'styled-components';
import { ContentWrapperSize } from '../ContentWrapper/ContentWrapper';

export interface IProductListProps {
    products: IFormattedProduct[];
}

export function ProductList(props: IProductListProps) {
    return (
        <VerticalMargin>
            {
                props.products.map(product => <ProductItem product={product} key={product.id} />)
            }
        </VerticalMargin>
    );
}

const VerticalMargin = styled.div`
    margin: ${ContentWrapperSize.NORMAL_VERTICAL} 0;
`;