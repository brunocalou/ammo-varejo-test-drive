import * as React from 'react';
import styled from 'styled-components';
import { Row } from '../../Row/Row';
import { IFormattedProduct } from '../../../../shared/formatted/iformatted-product.d';
import { Theme } from '../../../theme/Theme';
import { Column } from '../../Column/Column';
import { FW } from '../../../style/FontWeight';

const ITEM_HEIGHT = 90;
const ITEM_HEIGHT_IN_PX = ITEM_HEIGHT + 'px';
const ITEM_PADDING = 4;
const ITEM_PADDING_IN_PX = ITEM_PADDING + 'px';
const IMAGE_SIZE_IN_PX = (ITEM_HEIGHT - 2 * ITEM_PADDING) + 'px';

export interface IProductItemProps {
    product: IFormattedProduct;
}

export function ProductItem(props: IProductItemProps) {
    return (
        <Item>
            <ImagesContainer>
                {
                    props.product.images.map((image, index) => <img key={index} src={image} />)
                }
            </ImagesContainer>

            <Content>
                <Column>
                    <Title>{props.product.name}</Title>
                    <SubTitle>{
                        props.product.tags
                            .map(tag => tag.name)
                            .reduce((acc, current) => acc + ' - ' + current)
                    }</SubTitle>
                </Column>

                <Price>
                    {
                        props.product.price.hasDiscount
                            ? <LightPrice><LineThrough>{props.product.price.basePrice}</LineThrough> por </LightPrice>
                            : <></>
                    }
                    {props.product.price.finalPrice}
                </Price>
            </Content>
        </Item>
    );
}

const Content = styled(Row)`
    width: 100%;
    padding: 0 20px;
`;

const Title = styled.span`
    font-size: 1em;
    font-weight: ${FW.normal};
    margin: 5px 0;
`;

const SubTitle = styled.span`
    font-size: 0.8em;
    font-weight: ${FW.light};
    color: ${Theme.textLightColor};
    margin: 0;
`;

const Price = styled(Title)`
    font-size: 0.9em;
    display: inline;
`;

const LightPrice = styled(Price)`
    color: ${Theme.textLightColor};
`;

// See https://stackoverflow.com/questions/8215754/line-through-not-centered-through-the-text-css
const LineThrough = styled.span`
    position: relative;
    white-space: nowrap;
    &:after {
        border-top: 1px solid ${Theme.textLightColor};
        position: absolute;
        content: "";
        right: 0;
        top:50%;
        left: 0;
    }
`;

const Item = styled(Row)`
    padding: ${ITEM_PADDING_IN_PX};
    border-bottom: 1px solid ${Theme.dividerColor};
    background-color: ${Theme.cardColor};
`;

const ImagesContainer = styled(Row)`
    height: 100%;
    display: flex;
    flex-direction: row;

    img {
        width: ${IMAGE_SIZE_IN_PX};
        height: ${IMAGE_SIZE_IN_PX};
        margin: 0 7px;
        background-color: grey;

        &:first-child {
            margin-left: 0;
        }
    }
`;