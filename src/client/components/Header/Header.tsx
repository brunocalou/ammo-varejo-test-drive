import * as React from 'react';
import styled from 'styled-components';
import { ContentWrapperSize } from '../ContentWrapper/ContentWrapper';
import { Theme } from '../../theme/Theme';
import { FW } from '../../style/FontWeight';
import { Row } from '../Row/Row';

export interface IHeaderProps {
    title: string;
    children?: React.ReactNode;
    onTitleClick?: () => any;
}

export function Header(props: IHeaderProps) {
    return (
        <Navbar>
            <Title onClick={props.onTitleClick}>{props.title}</Title>
            {props.children}
        </Navbar>
    );
}

const Title = styled.h3`
    font-size: 1.5em;
    color: ${Theme.headerTitleColor};
    padding: 0;
    margin: 0;
    font-weight: ${FW.bold};
    cursor: pointer;
`;

const Navbar = styled(Row)`
    background-color: ${Theme.headerColor};
    padding: 0 ${ContentWrapperSize.SMALL_HORIZONTAL};
    height: 64px;
`;