import * as React from 'react';
import styled from 'styled-components';
import { Theme } from '../../theme/Theme';
import { Pagination } from '../../../shared/pagination';
import { Row } from '../Row/Row';
import { MdSkipPrevious, MdChevronLeft, MdSkipNext, MdChevronRight } from 'react-icons/md';
import { IconTheme } from '../../theme/IconTheme';
import { FW } from '../../style/FontWeight';

export interface IPaginationItemsProps {
    pagination: Pagination;
    maxPagesToShow: number;
    onPageChange: (page: number) => any;
}

function isPreviousDisabled(pagination: Pagination) {
    return pagination.page === 1;
}

function isNextDisabled(pagination: Pagination) {
    return pagination.page === pagination.pageCount;
}

function generatePages(props: IPaginationItemsProps) {
    const pageNumbers = [];
    const { pageCount, page } = props.pagination;
    const { maxPagesToShow } = props;

    if (pageCount < page + maxPagesToShow) {
        let start = pageCount - maxPagesToShow;
        if (start < 1) {
            start = 1;
        }
        for (let i = pageCount; i >= start; i -= 1) {
            pageNumbers.unshift(i);
        }
    } else {
        for (let i = page; i < Math.min(page + maxPagesToShow, pageCount); i += 1) {
            pageNumbers.push(i);
        }
    }

    return pageNumbers.map(num =>
        <Page
            key={num}
            selected={num === page}
            onClick={() => {
                if (num !== page) {
                    props.onPageChange(num);
                }
            }}
        >
            {num}
        </Page>
    );
}

export function PaginationItems(props: IPaginationItemsProps) {
    const disabledPrevious = isPreviousDisabled(props.pagination);
    const disabledNext = isNextDisabled(props.pagination);

    return (
        <Row>
            <PageIcon
                disabled={disabledPrevious}
                onClick={() => props.onPageChange(1)}>
                <MdSkipPrevious size={IconTheme.bigSize.width} />
            </PageIcon>

            <PageIcon
                disabled={disabledPrevious}
                onClick={() => props.onPageChange(props.pagination.page - 1)}>
                <MdChevronLeft size={IconTheme.bigSize.width} />
            </PageIcon>

            {generatePages(props)}

            <PageIcon
                disabled={disabledNext}
                onClick={() => props.onPageChange(props.pagination.page + 1)}>
                <MdChevronRight size={IconTheme.bigSize.width} />
            </PageIcon>

            <PageIcon
                disabled={disabledNext}
                onClick={() => props.onPageChange(props.pagination.pageCount)}>
                <MdSkipNext size={IconTheme.bigSize.width} />
            </PageIcon>
        </Row>
    );
}

interface IPageProps {
    selected?: boolean;
}

const Page = styled.button`
    font-size: 1em;
    color: ${Theme.textLightColor};
    font-weight: ${FW.normal};
    border-radius: 2px;
    border: ${(props: IPageProps) => props.selected ? `1px solid ${Theme.borderColor}` : '0'};
    background-color: ${(props: IPageProps) => props.selected ? Theme.cardColor : 'transparent'};
    padding: 0.5em ${(props: IPageProps) => props.selected ? '1.2em' : '0.6em'};
    margin: 0 ${(props: IPageProps) => props.selected ? '0.5em' : '0'};

    &:not(:disabled) {
        cursor: pointer;
    }
`;

const PageIcon = styled(Page)`
    padding: 0.5em 0.7em;
    color: ${IconTheme.color};

    &:disabled {
        color: ${Theme.disabledColor};
    }
`;