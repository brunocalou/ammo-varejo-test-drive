import * as React from 'react';
import styled from 'styled-components';
import { ContentWrapperSize } from '../ContentWrapper/ContentWrapper';
import { Theme } from '../../theme/Theme';
import { FW } from '../../style/FontWeight';
import { Row } from '../Row/Row';

export interface ISubHeaderProps {
    title: string;
    children?: React.ReactNode;
}

export function SubHeader(props: ISubHeaderProps) {
    return (
        <Navbar>
            <Title>{props.title}</Title>
            {props.children}
        </Navbar>
    );
}

const Title = styled.h1`
    font-size: 2em;
    color: ${Theme.subHeaderTitleColor};
    padding: 0;
    margin: 0;
    font-weight: ${FW.lighter};
`;

const Navbar = styled(Row)`
    background-color: ${Theme.subHeaderColor};
    padding: 0 ${ContentWrapperSize.SMALL_HORIZONTAL};
    height: 90px;
    display: flex;
    align-items: center;
`;