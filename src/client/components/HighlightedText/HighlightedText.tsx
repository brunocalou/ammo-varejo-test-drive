import styled from 'styled-components';
import { Theme } from '../../theme/Theme';

export const HighlightedText = styled.h5`
    text-transform: uppercase;
    padding: 0.5em 0;
    margin: 0;
    border-bottom: 3px solid ${Theme.underlineHighlightColor};
    display: inline-block;
`;