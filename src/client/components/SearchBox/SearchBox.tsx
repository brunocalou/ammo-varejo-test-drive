import * as React from 'react';

import styled from 'styled-components';
import { Theme } from '../../theme/Theme';

import { MdSearch } from 'react-icons/md';
import { FaTimesCircle } from 'react-icons/fa';
import { IconTheme } from '../../theme/IconTheme';
import { IconSize } from '../../util/IconSize';

export interface ISearchBoxProps {
  placeholder?: string;
  onSearch: (value: string) => any;
}

export interface ISearchBoxState {
  value: string;
}

export default class SearchBox extends React.Component<ISearchBoxProps, ISearchBoxState> {
  constructor(props: ISearchBoxProps) {
    super(props);

    this.state = {
      value: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }

  public render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <Box>
          <Button onClick={this.handleSubmit}>
            <MdSearch color={IconTheme.color} width={IconSize.normal.width} height={IconSize.normal.height} />
          </Button>

          <Input placeholder={this.props.placeholder} value={this.state.value} onChange={this.handleChange} />

          <Button onClick={this.resetForm}>
            <FaTimesCircle color={IconTheme.color} width={IconSize.normal.width} height={IconSize.normal.height} />
          </Button>
        </Box>
      </form>
    );
  }

  private handleChange(event) {
    this.setState({ value: event.target.value });
  }

  private handleSubmit(event) {
    event.preventDefault();
    this.props.onSearch(this.state.value);
  }

  private resetForm(event) {
    this.setState({ value: '' });
  }
}

const Box = styled.div`
    border-radius: 50px;
    border: 1px solid ${Theme.borderColor};
    padding: 2px;
    background-color: ${Theme.searchBoxColor};
    width: 400px;
    height: 20px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const Input = styled.input`
    border: 0;
    width: 100%;
    height: 100%;
    font-size: 0.9em;
`;

const Button = styled.div`
    border: 0;
    height: 100%;
    padding: 5px 15px;
    cursor: pointer;
    display: flex;

    > * {
    display: block;
    margin: auto;
    }
`;