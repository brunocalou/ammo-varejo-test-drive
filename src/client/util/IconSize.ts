import { Size } from './Size';

export class IconSize {
    public static normal = Size.square(14);
    public static big = Size.square(24);
}