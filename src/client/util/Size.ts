export class Size {
    public static square(dimension: number) {
        return new Size(dimension, dimension);
    }

    constructor(public width: number, public height: number) { }
}