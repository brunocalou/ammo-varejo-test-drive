import { IFormattedProduct } from '../../shared/formatted/iformatted-product.d';
import { Response } from '../../shared/response';
import axios, { AxiosPromise } from 'axios';
import { ISearchParams } from './ProductsApi';

export const searchLimits = [20, 40, 60, 80, 100];

export interface ISearchParams {
    search: string;
    page: number;
    limit: number;
}

class ProductsApiImpl {
    public getProducts(params: ISearchParams): AxiosPromise<Response<IFormattedProduct[]>> {
        const { search, page, limit } = this.normalizeParams(params);
        return axios.get<Response<IFormattedProduct[]>>(`/api/products?page=${page}&limit=${limit}&search=${search}`);
    }

    public normalizeParams(params: ISearchParams): ISearchParams {
        return {
            search: (params.search || '').split(' ').join('+'),
            page: params.page || 1,
            limit: params.limit || searchLimits[0],
        };
    }
}

export const ProductsApi = new ProductsApiImpl();