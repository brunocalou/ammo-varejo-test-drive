export class ProductCategory {
    constructor(
        public readonly name: string,
        public readonly id?: number,
    ) { }
}