export class Price {
    constructor(
        public readonly basePrice: number,
        public readonly discountPercentage: number,
        public readonly id?: number,
    ) { }

    get hasDiscount(): boolean {
        return this.discountPercentage !== 0;
    }

    get finalPrice(): number {
        return this.basePrice * this.discountPercentage / 100;
    }
}