import { ProductTag } from './product-tag';
import { Price } from './price';
import { ProductCategory } from './product-category';
import { IDateAwareModel } from './idate-aware-model';

interface IProductConstructorParams {
    readonly name: string;
    readonly images: string[];
    readonly categories: ProductCategory[];
    readonly price: Price;
    readonly keywords: string;
    readonly tags: ProductTag[];
    readonly id?: number;
    readonly creationDate?: Date;
    readonly updateDate?: Date;
}

export class Product extends IDateAwareModel {
    public readonly name: string;
    public readonly images: string[];
    public readonly categories: ProductCategory[];
    public readonly price: Price;
    public readonly keywords: string;
    public readonly tags: ProductTag[];

    constructor(
        params: IProductConstructorParams
    ) {
        super(params.id, params.creationDate, params.updateDate);
        this.name = params.name;
        this.images = params.images;
        this.categories = params.categories;
        this.price = params.price;
        this.keywords = params.keywords;
        this.tags = params.tags;
    }
}