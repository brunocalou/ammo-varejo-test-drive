export class ProductTag {
    constructor(
        public readonly name: string,
        public readonly id?: number,
    ) { }
}