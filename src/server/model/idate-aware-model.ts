export abstract class IDateAwareModel {
    constructor(
        public readonly id?: number,
        public readonly creationDate?: Date,
        public readonly updateDate?: Date,
    ) { }
}