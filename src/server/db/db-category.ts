import { IDb } from './idb.d';
import { ProductCategory } from '../model/product-category';

export class DbCategory {
    constructor(private db: IDb) { }

    public async createCategory(name: string): Promise<ProductCategory> {
        const { rows } = await this.db.query(`
            INSERT INTO product_category (name)
            VALUES ($1)
            RETURNING *;
        `, [name]);
        console.log('---=----');
        console.log(rows);
        return rows[0];
    }
}