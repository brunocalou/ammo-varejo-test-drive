/**
 * @see {@link https://node-postgres.com/guides/project-structure}
 */

import { IDb } from './idb';

const { Pool } = require('pg');

const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
});

export const db: IDb = {
  query: async (text, params) => {
    const start = Date.now();
    const result = await pool.query(text, params);
    const duration = Date.now() - start;

    console.log('executed query', { text, duration, rows: result.rowCount });
    return result;
  }
};