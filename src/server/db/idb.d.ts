export interface IDb {
    query(text: string, params: any[]): any;
}