import { Product } from '../model/product';
import { ProductFormatter } from '../formatters/product-formatter';
import { Price } from '../model/price';
import { ProductTag } from '../model/product-tag';
import { ProductCategory } from '../model/product-category';
import { IDb } from './idb';
import { IFormattedProduct } from '../../shared/formatted/iformatted-product';
import { constrain } from '../util/constrain';
import { Response } from '../../shared/response';
import { Pagination } from '../../shared/pagination';
import { SearchMetrics } from '../../shared/search-metrics';

export class DbProducts {
    public static MIN_ITEMS_PER_PAGE = 10;
    public static MAX_ITEMS_PER_PAGE = 100;

    private static calculatePageCount(size: number, limit: number) {
        return Math.max(Math.ceil(size / limit), 1);
    }

    private static calculateAndCheckPageCount(size: number, limit: number, page: number): number {
        const pageCount = this.calculatePageCount(size, limit);

        if (page > pageCount) {
            throw new Error(`Page (${page}) cannot be bigger than pageCount (${pageCount})`);
        }

        return pageCount;
    }

    constructor(private db: IDb) { }

    public async isEmpty(): Promise<boolean> {
        const { rows } = await this.db.query(`SELECT id
            FROM product
            LIMIT 1;
        `, []);

        return rows.length === 0;
    }

    public async getSize(): Promise<number> {
        const { rows } = await this.db.query(`
            SELECT COUNT(*) FROM product;
        `, []);
        return rows[0].count;
    }

    public async createProduct(product: Product): Promise<number> {
        // Create price
        let priceId = product.price.id;
        if (priceId === null || priceId === undefined) {
            const { rows } = await this.db.query(`
                INSERT INTO price ("basePrice", "discountPercentage") VALUES ($1, $2)
                RETURNING *;
            `, [product.price.basePrice, product.price.discountPercentage]);

            priceId = rows[0].id;
        }

        // Create product
        const productId = (await this.db.query(`
            INSERT INTO product (name, images, "priceId", keywords)
            VALUES ($1, $2, $3, $4)
            RETURNING id;
        `, [product.name, product.images, priceId, product.keywords])).rows[0].id;

        // Relate product and categories
        await Promise.all(product.categories.map(async category => {
            return await this.db.query(`
                INSERT INTO product_category_relation ("productId", "productCategoryId")
                VALUES ($1, $2)
            `, [productId, category.id]);
        }));

        // Relate product and tags
        await Promise.all(product.tags.map(async tag => {
            return await this.db.query(`
                INSERT INTO product_tag_relation ("productId", "productTagId")
                VALUES ($1, $2)
            `, [productId, tag.id]);
        }));

        return productId;
    }

    public async getProducts(page: number, itemsPerPage: number): Promise<Response<IFormattedProduct[]>> {
        const limit = constrain(itemsPerPage, DbProducts.MIN_ITEMS_PER_PAGE, DbProducts.MAX_ITEMS_PER_PAGE);
        const size = await this.getSize();
        const pageCount = DbProducts.calculateAndCheckPageCount(size, limit, page);

        const { rows } = await this.db.query(`
            SELECT prod.id, prod.name, cat.categories, cat.cat_ids as "categoryIds", tag.tags, tag.tag_ids as "tagIds", price.id as priceId, images, keywords, "basePrice", "discountPercentage"
            FROM   product prod
            JOIN  (
                SELECT pcr."productId" AS id, array_agg(cat.name) AS categories, array_agg(cat.id) AS cat_ids
                FROM   product_category_relation pcr
                JOIN   product_category cat ON cat.id = pcr."productCategoryId"
                GROUP  BY pcr."productId"
            ) cat USING (id)
            JOIN  (
                SELECT ptr."productId" AS id, array_agg(tag.name) AS tags, array_agg(tag.id) AS tag_ids
                FROM   product_tag_relation ptr
                JOIN   product_tag tag ON tag.id = ptr."productTagId"
                GROUP  BY ptr."productId"
            ) tag USING (id)
            JOIN price
            ON prod."priceId"=price.id
            ORDER BY prod."updateDate"
            LIMIT $1
            OFFSET $2;
        `, [limit, this.calculateOffset(page, limit)]);

        const formattedProducts = rows.map(row => this.formatProductsRow(row));

        return new Response({
            data: formattedProducts,
            pagination: new Pagination({
                page,
                pageCount
            }),
            metrics: new SearchMetrics({
                resultsCount: size
            }),
        });
    }

    public async searchProducts(search: string, page: number, itemsPerPage: number): Promise<Response<IFormattedProduct[]>> {
        // TODO: Cache the query search to avoid performance issues
        const limit = constrain(itemsPerPage, DbProducts.MIN_ITEMS_PER_PAGE, DbProducts.MAX_ITEMS_PER_PAGE);
        const querySearch = search.split(' ').join('&');

        const { rows } = await this.db.query(`
            SELECT prod_id as id, prod_name as name, cats as categories, "categoryIds", tags, "tagIds", priceId, images, keywords, "basePrice", "discountPercentage"
            FROM (SELECT
                    prod.id as prod_id,
                    prod.name as prod_name,
                    cat.categories as cats,
                    cat.cat_ids as "categoryIds",
                    tag.tags as tags,
                    tag.tag_ids as "tagIds",
                    price.id as priceId,
                    images,
                    keywords,
                    "basePrice",
                    "discountPercentage",
                        prod."updateDate" as prod_update_date,
                    to_tsvector(unaccent(prod.keywords)) ||
                    to_tsvector(unaccent(prod.name)) as document
                FROM product prod
                JOIN  (
                    SELECT pcr."productId" AS id, array_agg(cat.name) AS categories, array_agg(cat.id) AS cat_ids
                    FROM   product_category_relation pcr
                    JOIN   product_category cat ON cat.id = pcr."productCategoryId"
                    GROUP  BY pcr."productId"
                ) cat USING (id)
                JOIN  (
                    SELECT ptr."productId" AS id, array_agg(tag.name) AS tags, array_agg(tag.id) AS tag_ids
                    FROM   product_tag_relation ptr
                    JOIN   product_tag tag ON tag.id = ptr."productTagId"
                    GROUP  BY ptr."productId"
                ) tag USING (id)
                JOIN price
                ON prod."priceId"=price.id
            ) p_search
            WHERE p_search.document @@ to_tsquery(unaccent($1))
            ORDER BY prod_update_date;
        `, [querySearch]);

        const pageCount = DbProducts.calculateAndCheckPageCount(rows.length, limit, page);

        const start = this.calculateOffset(page, limit);
        const end = start + limit + 1;

        const formattedProducts = rows.slice(start, end).map(row => this.formatProductsRow(row));

        return new Response({
            data: formattedProducts,
            pagination: new Pagination({
                page,
                pageCount
            }),
            metrics: {
                resultsCount: rows.length
            }
        });
    }

    private formatProductsRow(row): IFormattedProduct {
        const tags = row.tags.map((tag, index) => new ProductTag(tag, row.tagIds[index]));
        const categories = row.categories.map((category, index) => new ProductCategory(category, row.categoryIds[index]));
        const price = new Price(row.basePrice, row.discountPercentage, row.priceId);
        const product = new Product({
            ...row,
            categories,
            tags,
            price,
        });
        return ProductFormatter.format(product);
    }

    private calculateOffset(page: number, limit: number) {
        return (page - 1) * limit;
    }
}