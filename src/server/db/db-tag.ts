import { IDb } from './idb.d';
import { ProductTag } from '../model/product-tag';

export class DbTag {
    constructor(private db: IDb) { }

    public async createTag(name: string): Promise<ProductTag> {
        const { rows } = await this.db.query(`
            INSERT INTO product_tag (name)
            VALUES ($1)
            RETURNING *;
        `, [name]);
        return rows[0];
    }
}