import { DbProducts } from './db-products';
import { DbCategory } from './db-category';
import { DbTag } from './db-tag';
import { Product } from '../model/product';
import { Price } from '../model/price';
import { IDb } from './idb';
import { RandomUtil } from '../util/random-util';

const MAX_PRICE = 400000;
const MIN_PRICE = 100;
const MAX_DISCOUNT_PRECENTAGE = 80;
const IMAGES_PER_PRODUCT = 4;
const TAGS_PER_PRODUCT = 4;
const KEYWORDS_PER_PRODUCT = 8;
const DB_NAME = 'ammo-varejo-test-drive-db';

const categories = [
    'Quarto',
];

const tags = [
    'Classic I',
    'Solteiro Extra',
    'King Size',
    'Queen Size',
    'Confortável',
    'Casal',
];

const keywords = [
    'colchão',
    'travesseiro',
    'edredom',
    'colcha',
    'lençol',
    'avulso',
    'promoção',
    'fronha',
    'coberta',
    'cobertor',
    'conforto',
    'confortável',
    'macio',
    'duro',
    'ergonômico',
    'leve',
    'box',
    'kit',
    'grande',
    'médio',
    'pequeno',
    'preto',
    'branco',
    'colorido',
];

const names = [
    'Cama Box',
    'Kit de cama 210 fios',
    'Kit de cama 180 fios',
    'Kit de cama 160 fios',
    'Cama completa',
    'Colchão',
    'Cama sob medida',
];

const images = [
    'https://www.ikea.com/pt/pt/images/products/songesand-estrutura-de-cama-castanho__0550502_PE658189_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/songesand-estrutura-de-cama-castanho__0550504_PE658191_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/songesand-estrutura-de-cama-castanho__0550503_PE658190_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/songesand-estrutura-cama-c-caixas-arrumacao-castanho__0552061_PE658850_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/songesand-estrutura-cama-c-caixas-arrumacao-castanho__0627014_PH149404_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/orje-estrutura-cama-c-arrumacao-castanho__0381651_PE556368_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/flekke-cama-indiv-dupla-c-gav-branco__0461561_PE608748_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/flekke-cama-indiv-dupla-c-gav-branco__0461563_PE608746_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/brimnes-cama-indiv-dupla-c-gav-branco__0159176_PE315614_S4.JPG',
    'https://www.ikea.com/pt/pt/images/products/brimnes-cama-indiv-dupla-c-gav-branco__0216931_PE372939_S4.JPG',
];

export class DbMock {
    private static MAX_NUMBER_OF_PRODUCTS = 100;

    constructor(private db: IDb) { }

    public async dbExists(): Promise<boolean> {
        const { rows } = await this.db.query(`
                SELECT 1
                FROM pg_database
                WHERE datname=$1;
            `, [DB_NAME]);
        return rows.length === 1;
    }

    public async fillDatabaseIfEmpty() {
        if (await this.isEmpty()) {
            await this.fillDatabase(DbMock.MAX_NUMBER_OF_PRODUCTS);
        }
    }

    public async isEmpty(): Promise<boolean> {
        return await new DbProducts(this.db).isEmpty();
    }

    public async fillDatabase(numberOfProducts: number) {
        const dbCategory = new DbCategory(this.db);
        const dbTag = new DbTag(this.db);
        const dbProducts = new DbProducts(this.db);

        const createdCategories = await Promise.all(categories.map(async category => await dbCategory.createCategory(category)));
        const createdTags = await Promise.all(tags.map(async tag => await dbTag.createTag(tag)));

        for (let i = 0; i < numberOfProducts; i += 1) {
            const product = new Product({
                name: RandomUtil.getRandomItem(names) as string,
                images: RandomUtil.getRandomItems(images, IMAGES_PER_PRODUCT),
                categories: createdCategories,
                price: new Price(Math.min(MIN_PRICE, Math.floor(Math.random() * MAX_PRICE)), Math.floor(Math.random() * MAX_DISCOUNT_PRECENTAGE)),
                keywords: RandomUtil.getRandomItems(keywords, KEYWORDS_PER_PRODUCT).join(' '),
                tags: RandomUtil.getRandomItems(createdTags, TAGS_PER_PRODUCT),
            });
            await dbProducts.createProduct(product);
        }
    }
}
