-- Tables --

CREATE TABLE price (
  id SERIAL NOT NULL PRIMARY KEY,
  "basePrice" INTEGER NOT NULL CHECK ("basePrice" >= 0),
  "discountPercentage" INTEGER NOT NULL CHECK ("discountPercentage" >= 0 AND "discountPercentage" < 100)
);

CREATE TABLE product_category (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT
);

CREATE TABLE product_tag (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE product (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  images TEXT[] NOT NULL,
  "priceId" INTEGER NOT NULL REFERENCES price (id),
  keywords TEXT NOT NULL,
  "creationDate" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  "updateDate" TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE product_category_relation (
  "productId" INTEGER NOT NULL REFERENCES product (id),
  "productCategoryId" INTEGER NOT NULL REFERENCES product_category (id)
);

CREATE TABLE product_tag_relation (
  "productId" INTEGER NOT NULL REFERENCES product (id),
  "productTagId" INTEGER NOT NULL REFERENCES product_tag (id)
);

-- Functions --

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Triggers --

CREATE TRIGGER set_timestamp_for_product
BEFORE UPDATE ON product
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

-- Extensions --

CREATE EXTENSION unaccent;