export class RandomUtil {
    public static getRandomItem(array: any[]): any {
        const randomIndex = Math.floor(Math.random() * (array.length - 1));
        return array[randomIndex];
    }

    public static getRandomItems(array: any[], numberOfItems: number): any[] {
        const result = [];

        for (let i = 0; i < numberOfItems; i += 1) {
            result.push(this.getRandomItem(array));
        }

        return result;
    }
}