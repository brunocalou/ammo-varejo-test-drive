import { IFormattedPrice } from '../../shared/formatted/iformatted-price';
import { Price } from '../model/price';
import { CurrencyFormatter } from './currency-formatter';

const currencyLocale = {
    locale: 'pt-BR',
    currency: 'BRL'
};

export class PriceFormatter {
    public static format(price: Price): IFormattedPrice {
        return {
            basePrice: CurrencyFormatter.format(price.basePrice / 100, currencyLocale.locale, currencyLocale.currency),
            finalPrice: CurrencyFormatter.format(price.finalPrice / 100, currencyLocale.locale, currencyLocale.currency),
            hasDiscount: price.hasDiscount,
        };
    }
}