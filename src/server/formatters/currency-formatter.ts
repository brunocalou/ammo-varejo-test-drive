export class CurrencyFormatter {
    public static format(value: number, locale: string|string[], currency: string): string {
        return value.toLocaleString(locale, { style: 'currency', currency});
    }
}