import { IFormattedProduct } from '../../shared/formatted/iformatted-product.d';
import { PriceFormatter } from './price-formatter';
import { Product } from '../model/product';

export class ProductFormatter {
    public static format(product: Product): IFormattedProduct {
        return {
            id: product.id,
            name: product.name,
            images: product.images,
            tags: product.tags,
            price: PriceFormatter.format(product.price),
            categories: product.categories,
        };
    }
}