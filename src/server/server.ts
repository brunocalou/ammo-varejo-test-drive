import * as express from 'express';
import { apiRouter } from './routes/api-router';
import { staticsRouter } from './routes/statics-router';
import { staticsDevRouter } from './routes/statics-dev-router';
import * as config from './config';
import { DbMock } from './db/db-mock';
import { db } from './db/db';
import * as paginate from 'express-paginate';
import { DbProducts } from './db/db-products';

const dbMock = new DbMock(db);

dbMock.fillDatabaseIfEmpty();

const app = express();

app.use(paginate.middleware(DbProducts.MIN_ITEMS_PER_PAGE, DbProducts.MAX_ITEMS_PER_PAGE));
app.use(apiRouter());
app.use(config.IS_PRODUCTION ? staticsRouter() : staticsDevRouter());

app.listen(config.SERVER_PORT, () => {
  console.log(`App listening on port ${config.SERVER_PORT}!`);
});