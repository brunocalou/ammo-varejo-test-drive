import * as bodyParser from 'body-parser';
import { Router } from 'express';
import { products } from './api/products';

export function apiRouter() {
  const router = Router();
  router.use(bodyParser.json());
  router.use('/api/products', products);

  return router;
}
