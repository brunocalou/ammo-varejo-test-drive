import Router from 'express-promise-router';
import { db } from '../../db/db';
import { DbProducts } from '../../db/db-products';

const router = Router();

router.get('/', async (req, res) => {
    const dbProducts = new DbProducts(db);
    const page = req.query.page;
    const limit = req.query.limit;
    const search = req.query.search;
    const isSearch = search !== undefined && search !== '';

    try {
        const retrievedProducts = isSearch ? await dbProducts.searchProducts(search, page, limit) : await dbProducts.getProducts(page, limit);
        res.send(retrievedProducts);
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
});

export const products = router;
