import { Pagination } from './pagination';
import { SearchMetrics } from './search-metrics';

interface IResponseParams<T> {
    data: T;
    pagination?: Pagination;
    metrics?: SearchMetrics;
}
export class Response<T> {
    public readonly data: T;
    public readonly pagination: Pagination;
    public readonly metrics: SearchMetrics;

    constructor(params: IResponseParams<T>) {
        this.data = params.data;
        this.pagination = params.pagination;
        this.metrics = params.metrics;
    }
}