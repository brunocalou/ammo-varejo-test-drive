interface IPaginationParams {
    page: number;
    pageCount: number;
}

export class Pagination {
    public page: number;
    public pageCount: number;

    constructor(params: IPaginationParams) {
        Object.assign(this, params);
    }
}