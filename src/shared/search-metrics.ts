export interface ISearchMetricsParams {
    resultsCount: number;
}
export class SearchMetrics {
    public readonly resultsCount: number;

    constructor(params: ISearchMetricsParams) {
        this.resultsCount = params.resultsCount;
    }
}