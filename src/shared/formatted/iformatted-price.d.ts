export interface IFormattedPrice {
    readonly basePrice: string;
    readonly finalPrice: string;
    readonly hasDiscount: boolean;
}