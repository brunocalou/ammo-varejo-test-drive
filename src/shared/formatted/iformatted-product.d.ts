import { IFormattedPrice } from './iformatted-price';
import { ProductTag } from '../../server/model/product-tag';
import { ProductCategory } from '../../server/model/product-category';

export interface IFormattedProduct {
    readonly id: number;
    readonly name: string;
    readonly images: string[];
    readonly tags: ProductTag[];
    readonly categories: ProductCategory[];
    readonly price: IFormattedPrice;
}