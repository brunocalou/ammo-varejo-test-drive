# Ammo Varejo Test Drive

The project is available at https://ammo-varejo-test-drive.herokuapp.com and hosted at https://bitbucket.org/brunocalou/ammo-varejo-test-drive/

## Getting started

### Prerequisites
1. [Git](https://git-scm.com/)
1. [Node](https://nodejs.org/en/)
1. [Postgres](https://www.postgresql.org/)
1. [Heroku](https://www.heroku.com) (optional)

### Setup

1. Clone the repository
    ```bash
    git clone git@bitbucket.org:brunocalou/ammo-varejo-test-drive.git
    ```

1. Install the dependencies
    ```bash
    npm install
    ```

1. Create a Postgres database
    ```bash
    createdb <db-name>
    ```

1. If your database is not owned by Heroku, perform a migration
    ```bash
    DATABASE_URL=postgresql://<db-user>:<db-user-password>@<host>:<port>/<db-name> npm run migrate:db
    ```

1. Copy the `.env-template` file, rename it to  `.env` file, and edit its content
    ```bash
    cp .env-template .env
    # edit the content using your favorite editor
    ```

### Running

1. `npm run dev`
1. ...
1. Profit!

### Deploying to Heroku

Follow the [instructions](https://devcenter.heroku.com/articles/git) to create a Heroku app, then add the [Postgres Addon](https://www.heroku.com/postgres)

There is a pipeline configured on bitbucket to deploy the project when the master branch changes. If you would like to push the repository directly to Heroku, follow the steps on [Heroku's article to deploy with git](https://devcenter.heroku.com/articles/git)

After deploying, you should check if the build was successful. If it failed, tell Heroku to [skip pruning](https://devcenter.heroku.com/articles/nodejs-support#skip-pruning) the dev dependencies

```bash
heroku config:set NPM_CONFIG_PRODUCTION=false YARN_PRODUCTION=false -a <your-app-name>
```

If the build was successful, everything is ready! The database was migrated automagically and the server is up and running

ヽ(⌐■_■)ノ ♪♬
